﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            if (sender == id_sl_red)
            {
                labelMessagered.TextColor = Color.Red;
                String cadena = id_sl_red.Value.ToString();
                labelMessagered.Text = cadena;
            }
            if (sender == id_sl_verde)
            {
                labelMessageverde.TextColor = Color.Green;
                String cadena = id_sl_verde.Value.ToString();
                labelMessageverde.Text = cadena;
            }
            if (sender == id_sl_blue)
            {
                labelMessageblue.TextColor = Color.Blue;
                String cadena = id_sl_verde.Value.ToString();
                labelMessageblue.Text = cadena;
            }
            if (sender == id_sl_alpha)
            {
                labelMessagealpha.TextColor = Color.Yellow;
                String cadena = id_sl_alpha.Value.ToString();
                labelMessagealpha.Text = cadena;
            }


            id_bv_background.BackgroundColor = Color.FromRgba((int)id_sl_red.Value, (int)id_sl_verde.Value, (int)id_sl_blue.Value, (int)id_sl_alpha.Value);

        }

        async private void pasar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }
    }
}
